from git import Repo
import requests
import json
import sys
import os


LOCAL_REPO_PATH = '~/Documents/masters/repos/tutor/compphys2020'


def init_repo():
    repo = Repo(LOCAL_REPO_PATH)
    git = repo.git
    with open('users.json') as f:
        users = json.load(f)
    return git, users


def build_file_struc(tut, users):
    if not os.path.isdir('./{}'.format(tut)):
        os.mkdir('./{}'.format(tut))
        for user in users:
            os.mkdir('./{}/{}'.format(tut, user))
    else:
        for user in users:
            files = os.listdir('./{}/{}/'.format(tut, user))
            for f in files:
                os.remove('./{}/{}/{}'.format(tut, user, f))


def log_commits(git, tut, users):
    for user in users:
        with open('{}/{}/commit-log.txt'.format(tut, user), 'w') as out:
            for email in users[user]:
                commits = git.log('--stat', '--author', email)
                for commit in commits.split('\ncommit '):
                    if tut in commit:
                        out.write(commit)


def log_prs(git, tut, users):
    url = ('https://api.bitbucket.org/2.0/repositories/' +
           'tdietel/compphys2020/pullrequests?state=merged&pagelen=50&sort=-created_on&fields=' +
           'values.state,values.title,values.created_on,values.updated_on,values.rendered.description.raw,' +
           'values.links.commits,next')
    r = requests.get(url)
    response = json.loads(r.text)
    # with open('test.json', 'w') as f:
    #     json.dump(response, f)
    #     return
    for v in response['values']:
        commits_link = v['links']['commits']['href']
        commits_request = requests.get(
            commits_link + '?fields=values.author.raw,values.summary.raw,values.links')
        commits = json.loads(commits_request.text)
        if len(commits['values']) == 0:
            print('pr no commits')
            continue
        pr_author = commits['values'][0]['author']['raw']
        diff_link = commits['values'][0]['links']['diff']['href']
        commit_hash = diff_link.split('/')[-1]
        diff = git.show(commit_hash)

        it = 0
        while 'Merge: ' in diff:
            it += 1
            diff_link = commits['values'][it]['links']['diff']['href']
            commit_hash = diff_link.split('/')[-1]
            diff = git.show(commit_hash)

        if tut in diff:
            stud_number = pr_author.split(' <')[0]
            stud_email = pr_author.split(' <')[1][0:-1]
            for user in users:
                if user == stud_number or stud_email in users[user]:
                    with open('{}/{}/pr-log.txt'.format(tut, user), 'a') as out:
                        out.write(' \
                            {} \n\n \
                            {} \n \
                            title: {} \n \
                            created: {} \n \
                            last updated: {} \n \
                            number of updates: {} \n \
                            state: {} \n\n'
                            .format(
                            pr_author,
                            tut,
                            v['title'],
                            v['created_on'], 
                            v['updated_on'], 
                            len(commits['values']), 
                            v['state']))


def log_pipes(git, tut, users):
    url = ('https://api.bitbucket.org/2.0/repositories/tdietel/compphys2020/pipelines/?' +
           'pagelen=100&sort=-created_on&fields=values.target.commit.links,values.state.result,values.created_on,' +
           'values.build_seconds_used,values.build_number,values.result,values.creator.display_name,next,values.trigger.name')
    r = requests.get(url)
    response = json.loads(r.text)
    # with open('test.json', 'w') as f:
    #     json.dump(response, f)
    #     return
    for v in response['values']:
        try:
            commit_link = v['target']['commit']['links']['self']['href']
        except:
            print('pipe no commit')
            continue
        commit_request = requests.get(
            commit_link + '?fields=author,links.diff')
        commit = json.loads(commit_request.text)
        author = commit['author']['raw']
        diff_link = commit['links']['diff']['href']
        commit_hash = diff_link.split('/')[-1]
        try:    
            diff = git.show(commit_hash)
        except:
            print('pipe no diff')
            continue

        if tut in diff:
            stud_number = author.split(' <')[0]
            stud_email = author.split(' <')[1][0:-1]
            for user in users:
                if user == stud_number or stud_email in users[user]:
                    with open('{}/{}/pipe-log.txt'.format(tut, user), 'a') as out:
                        out.write(' \
                            {} \n\n \
                            {} \n \
                            created: {} \n \
                            build time (s): {} \n \
                            trigger: {} \n \
                            result: {} \n\n'
                            .format(
                            author, 
                            tut, 
                            v['created_on'], 
                            v['build_seconds_used'], 
                            v['trigger']['name'] + 'from commit ' + commit_hash,
                            v['state']['result']['name']))


def dl_artifacts(git, tut, users):
    for user in users:
        url = 'https://bitbucket.org/jbarrella/cp-2020/downloads/' + tut + user + '.tar.gz'
        # url = 'https://bitbucket.org/jbarrella/cp-2020/downloads/tut2_BRRJAS002.tar.gz'
        r = requests.get(url)
        with open('./{}/{}/{}.tar.gz'.format(tut, user, tut + '_' + user), 'wb') as tar:
            tar.write(r.content)


if __name__ == '__main__':
    try:
        tut = sys.argv[1]
    except:
        exit('usage "python pull_info.py <tutnumber>"')

    git, users = init_repo()
    build_file_struc(tut, users)

    log_commits(git, tut, users)
    log_prs(git, tut, users)
    log_pipes(git, tut, users)
    dl_artifacts(git, tut, users)
